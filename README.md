# README #

Vagrant setup for  Pitney Bowes Sectrum

### What is this repository for? ###


- build a reproducible VM running spectrum
    

### How do I get set up? ###

- following setup is required
    - spectrum 19.1 is downloaded unpacked into a subfolder spectrum
    - so /spectrum/installers-19.1-linux is available (large folder, ~8GB)
    - there is a folder /spectrum/license_key
    - put your license key for spectrum into it, vagrant will grab and register it 
    - `cp installer.properties spectrum/installers-19.1-linux/SilentInstaller/`

Then, `vagrant up`.
To start the Spectrum server, do `vagrant ssh` and then
```bash
su - dspectrum
# password is dspectrum
cd /opt/spectrum/server/bin
. ./setup && ./server.start
# takes like 5 minutes
# follow progress via
tail -f /opt/spectrum/server/logs/spectrum-server.log
```

### Contribution guidelines ###



### Who do I talk to? ###

### Misc ###
##### Syntax Highlighting Vagrantfile
Syntax highlighting for the Vagrantfile in IntelliJ IDEA Community Edition is possible by cloning [https://github.com/textmate/ruby.tmbundle] and installing it via the textmate bundles plugin.