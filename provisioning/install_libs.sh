#!/usr/bin/env bash
# install required base software
yum install -y libstdc++.i686 libstdc++-devel.i686 libstdc++-devel.x86_64 zlib.i686 zlib.x86_64
sudo yum -y install java-1.8.0-openjdk
sudo yum -y install java-1.8.0-openjdk-devel