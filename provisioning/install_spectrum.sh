#!/usr/bin/env bash
# spectrum silent installation to /opt/spectrum
sudo mkdir -p /opt/spectrum
sudo chown -R dspectrum:dspectrum /opt/spectrum

chmod a+x /vagrant_data/installers-19.1-linux/install.sh
cd /vagrant_data/installers-19.1-linux
sudo -u dspectrum bash install.sh -f /vagrant_data/installers-19.1-linux/SilentInstaller/installer.properties

sudo -u dspectrum cp /vagrant_data/license_key/*.key /opt/spectrum/server/import/