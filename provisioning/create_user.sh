#!/usr/bin/env bash
# dspectrum user, password=dspectrum
sudo useradd dspectrum
echo "dspectrum" | sudo passwd --stdin dspectrum
sudo usermod -aG wheel dspectrum
sudo usermod -aG root dspectrum
