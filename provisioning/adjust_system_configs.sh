#!/usr/bin/env bash
# pre-installation configuration
if ! grep -q "*               soft    nofile          65536" /etc/security/limits.conf; then
    sudo sed  -i "\$ i *               soft    nofile          65536" /etc/security/limits.conf
    sudo sed  -i "\$ i *               hard    nofile          131072" /etc/security/limits.conf
    sudo sed  -i "\$ i *               soft    nproc           4096" /etc/security/limits.conf
    sudo sed  -i "\$ i *               hard    nproc           65536" /etc/security/limits.conf
fi

if ! grep -q "vm.max_map_count = 262144" /etc/sysctl.conf; then
    sudo sed -i "$ a vm.max_map_count = 262144" /etc/sysctl.conf
fi
sudo sysctl -p

sudo sed -i '/*          soft    nproc     4096/d' /etc/security/limits.d/20-nproc.conf

ulimit -f 4194304